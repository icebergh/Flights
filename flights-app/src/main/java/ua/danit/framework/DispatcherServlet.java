package ua.danit.framework;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;

import ua.danit.framework.invocations.Invocation;


public class DispatcherServlet extends HttpServlet {

  Map<String, Invocation> resources = new HashMap<>();

  /**
   * DispatcherServlet.
   * This designer DispatchServlet constructor
   */

  public DispatcherServlet(Class<?>... classes) {
    for (Class<?> someClass : classes) {
      resources.putAll(Parser.getClassInvocations(someClass));
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String uri = req.getRequestURI();

    Invocation invoc = resources.get(uri);

    if (canHandleGetMethod(invoc)) {
      handleMethod(req, resp, invoc);
    } else {
      resp.sendError(404);
    }
  }

  private void handleMethod(HttpServletRequest req, HttpServletResponse resp, Invocation invoc)
      throws IOException {

    String result = hasQueryParams(req) ? getHttpResult(req, invoc) : invoc.invoke();
    resp.getWriter().write(result);
  }

  private String getHttpResult(HttpServletRequest req, Invocation invoc) {
    String result = null;

    for (QueryParam queryParam : invoc.getQueryParamList()) {
      String name = req.getParameter(queryParam.value());
      result = invoc.invoke(name);
    }

    return result;
  }

  private boolean hasQueryParams(HttpServletRequest req) {
    return req.getQueryString() != null;
  }

  private boolean canHandleGetMethod(Invocation invoc) {
    return invoc.getHttpMethod() instanceof GET;
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String uri = req.getRequestURI();
    Invocation invoc = resources.get(uri);

    if (canHandlePostMethod(invoc)) {
      handleMethod(req, resp, invoc);
    } else {
      resp.sendError(404);
    }
  }

  private boolean canHandlePostMethod(Invocation invoc) {
    return invoc.getHttpMethod() instanceof POST;
  }

}