package ua.danit.framework;

import static ua.danit.framework.utils.Reflections.getMethods;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import ua.danit.framework.invocations.Invocation;
import ua.danit.framework.utils.Reflections;


public class Parser {

  /**
   * getClassInvocations(Class<?> someClass).
   * This method returns maps with all invocations for the given class,
   * where value inside @Path annotation over the method represents key.
   */

  public static Map<String, Invocation> getClassInvocations(Class<?> someClass) {
    Map<String, Invocation> resources = new HashMap<>();

    for (Method method : getMethods(someClass)) {
      if (method.isAnnotationPresent(Path.class)) {
        Invocation invoc = getInvocation(someClass, method);
        resources.put(invoc.getUrl(), invoc);
      }
    }

    return resources;
  }

  /**
   * getInvocation(Class<?> someClass, Method method).
   * This method returns an object of the Class that
   * encapsulate method parameters and annotations taken by reflection API
   */

  public static Invocation getInvocation(Class<?> someClass, Method method) {
    Path path = (Path) Reflections.getAnnotation(method, Path.class);
    String uri = path.value();
    Annotation annotation = Reflections.getAnnotation(method, GET.class);
    if (annotation == null) {
      annotation = Reflections.getAnnotation(method, POST.class);
    }
    if (annotation != null) {
      List<QueryParam> some = new ArrayList<>();
      Annotation[][] res = method.getParameterAnnotations();

      for (Annotation[] re : res) {
        for (Annotation annotation1 : re) {
          if (annotation1 != null) {
            some.add((QueryParam) annotation1);
          }
        }
      }
      return some == null ? new Invocation(someClass, method, uri, annotation) :
          new Invocation(someClass, method, uri, annotation, some);

    }
    return new Invocation(someClass, method, uri);
  }
}
