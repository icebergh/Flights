package ua.danit.application;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.danit.framework.DispatcherServlet;

public class WebApp {

  /**
   * This method is the entry point of our application ...
   * creates a server, creates a handler, parser classes
   */

  public static void main(String[] args) throws Exception {

    Server server = new Server(8080);
    ServletContextHandler handler = new ServletContextHandler();

    DispatcherServlet servlet = new DispatcherServlet(Resource.class);

    handler.addServlet(new ServletHolder(servlet), "/*");
    server.setHandler(handler);
    server.start();
    server.join();
  }
}
