package ua.danit.application;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

public class Resource {
  public static final String HTML_TEMPLATE = "<!DOCTYPE html>\n"
      + "<html>\n<head><title>British Airlines</title></head>"
      + "<body><h1 style=\" text-align: center; \">%s</h1></body></html>";

  @Path("/")
  @GET
  public String welcome() {
    return sendResponse("Welcome to British Airlines");
  }

  @Path("/weather")
  @GET
  public String getWeather() {
    return sendResponse("Today weather is cool isn't");
  }

  @Path("/flights")
  @GET
  public String getFlights() {
    return sendResponse("Today all routes closed");
  }

  @Path("/tickets")
  @POST
  public String getTickets() {
    return sendResponse("No ticket available");
  }

  /**
   * This method returns a string.
   */

  @Path("/query")
  @GET
  public String getUsers(@QueryParam("user") String name) {

    return sendResponse("User name is : " + name);

  }


  private String sendResponse(String response) {
    return String.format(HTML_TEMPLATE, response);
  }
}
